# Websocket implemtert i Java


## Features
- Multithreading
- Close,ping,pong,continuation
- Meldinger på opptil 64KB kan leses
- Meldinger på opptil 2^32 byte kan sendes.
- Mavenprosjekt

## Kilder
- [RFC6455](https://tools.ietf.org/html/rfc6455)
- [Writing WebSocket servers, MDN](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_servers) 

## NB: Krever Java 8

# Eksempel

```java
public class Main implements WebSocketClientListener {
	private WebSocket web;

	public Main() {
		try {
			web = new WebSocket(45000);
			web.setListener(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onMessage(JSONObject obj) {
		System.out.println("Mottok en melding fra klient(er)");
		// TODO gjør noe
	}

	@Override
	public void onConnection(WebSocketClient client) {
		web.sendMessage("hei på deg");
	}

	public static void main(String a[]) {
		new Main();
	}
}


```