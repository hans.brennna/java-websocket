package aasbo;

import java.io.IOException;

import aasbo.json.JSONObject;
import aasbo.listeners.WebSocketClientListener;
import aasbo.socket.WebSocket;
import aasbo.socket.WebSocketClient;

/**
 * En klasse som kjører en basic test av websocketen.
 * @author ole-johannes
 *
 */
public class Main implements WebSocketClientListener {
	private WebSocket web;

	public Main() {
		try {
			web = new WebSocket(45000);
			web.setListener(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onMessage(JSONObject obj) {
		System.out.println("Mottok en melding fra klient(er)");
		// TODO gjør noe
	}

	@Override
	public void onConnection(WebSocketClient client) {
		web.sendMessage("hei på deg");
		
	}

	public static void main(String a[]) {
		new Main();
	}
}
