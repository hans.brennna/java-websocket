package aasbo.exceptions;


/**
 * 
 * @author hans Excepetion for å håndtere sending av pong ved ping.
 *
 */
public class PingException extends Exception {
	private byte[] pong = null;
	/**
	 * 
	 * @param message Error melding.
	 * @param pong bytes som skal sendes tilbake til klient som pong.
	 */
	public PingException(String message,byte [] pong){
		super(message);
		this.pong = pong;
	}
	
	/**
	 * 
	 * @return pong data
	 */
	public byte[]  getPong(){
		return pong;
	}
}
