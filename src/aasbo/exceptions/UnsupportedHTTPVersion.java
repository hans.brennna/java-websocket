package aasbo.exceptions;

public class UnsupportedHTTPVersion extends Exception{

	public UnsupportedHTTPVersion(String message){
		super(message);
	}
}
