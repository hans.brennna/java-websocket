package aasbo.exceptions;

public class UnsupportedPayloadDataException extends Exception {
	public UnsupportedPayloadDataException(String message){
		super(message);
	}
}
