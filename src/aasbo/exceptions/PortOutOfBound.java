package aasbo.exceptions;

import java.io.IOException;

public class PortOutOfBound extends IOException{

	public PortOutOfBound(String message){
		super(message);
	}
}
