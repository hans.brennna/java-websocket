package aasbo.exceptions;
/**
 * 
 * @author hans
 * Exception for å trigge pong callback i WebSocketClient.
 */
public class PongException extends Exception  {
	/**
	 * 
	 * @param message Melding for error.
	 */
	public PongException(String message){
		super(message);
	}
}
