package aasbo.exceptions;

public class NotMaskedException extends Exception {
	
	public NotMaskedException(String message){
		super(message);
	}

}
