package aasbo.exceptions;

public class TooLargeMessage extends Exception{

	public TooLargeMessage(String message){
		super(message);
	}
}
