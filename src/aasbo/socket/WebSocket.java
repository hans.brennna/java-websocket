package aasbo.socket;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import aasbo.exceptions.PortOutOfBound;
import aasbo.exceptions.TooLargeMessage;
import aasbo.exceptions.UnsupportedHTTPVersion;
import aasbo.frames.FrameWriter;
import aasbo.json.JSONObject;
import aasbo.listeners.ClientListener;
import aasbo.listeners.WebSocketClientListener;

/**
 * Denne klassen setter opp en websocket som gjør det mulig for flere klienter å koble til fra nettlesere
 * @author ole-johannes
 *
 */
public class WebSocket implements java.io.Closeable, ClientListener {
	private ServerSocket socket;
	private ArrayList<WebSocketClientListener> listener = new ArrayList<>();
	private ArrayList<WebSocketClient> klienter = new ArrayList<>();
	private Timer timer;
	private final Object arrayBlock = new Object();
	private final long pingRate = 1 * 10 * 1000;

	public WebSocket(int port) throws IOException {
		if (port < 0 || port > 0xFFFF)
			throw new PortOutOfBound("Port value out of range: " + port);
		socket = new ServerSocket(port);
		startSocket();
		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				System.out.println("Pinger " + klienter.size() + " klienter");
				sendMessage(FrameWriter.ping());
			}
		}, pingRate, pingRate);
	}

	/**
	 * Gjør det mulig for "brukere" (objekter) av WebSocket klassen å lytte på
	 * hendelser som skjer i WebSocketen
	 * 
	 * @param l Lytteren som skal registreres
	 */
	public synchronized void setListener(WebSocketClientListener l) {
		listener.add(l);
	}

	/**
	 * Sender en melding til alle klienter som er registret som aktive
	 * 
	 * @param text Meldingen/data som skal sendes
	 */
	public synchronized void sendMessage(String text) {
		System.out.println("Sender: " + text);
		byte[] bytes;
		try {
			bytes = FrameWriter.ConvertString(text);
			klienter.stream().forEach((client) -> {
				client.sendMessage(bytes);
			});
		} catch (TooLargeMessage e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sender data til alle klienter som er registret som aktive
	 * 
	 * @param data Data som skal sendes(må være riktig formatert)
	 */
	public synchronized void sendMessage(byte[] data) {
		klienter.stream().forEach((client) -> {
			client.sendMessage(data);
		});
	}

	/**
	 * Denne metoden aksepterer alle klienter somn prøver å koble seg opp og
	 * sender klientene vidre til en annen metode som tar seg av websocket delen
	 */
	private void startSocket() {
		new Thread() {
			@Override
			public void run() {
				while (true) {
					try {
						System.out.println("Venter på Klient");
						handleClientConnection(socket.accept());
					} catch (IOException e) {
						e.printStackTrace();
						break;
					}
				}
			}
		}.start();
	}

	/**
	 * Denne metoden tar seg av maskeringsnøkkelen
	 * 
	 * @param key nøkkel fra klienten
	 * @return ny nøkkel
	 */
	private String websocketAccept(String key) {
		String magicString = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
		String concat = key + magicString;

		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-1");
			byte[] res = digest.digest(concat.getBytes());
			Base64.Encoder encode = Base64.getEncoder();
			return encode.encodeToString(res);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}

	}

	/**
	 * Tar av seg alt det som trengs for at en klient skal kobles til tjeneren.
	 * Gjør alt i en ny tråd slik at det er mulig å akseptere nye klienter
	 * raskes mulig uten at det hoper seg opp queue
	 * 
	 * @param client Socketen til den nyoppkoblede klienten
	 */
	private void handleClientConnection(Socket client) {
		new Thread() {
			@Override
			public void run() {
				InputStreamReader readConnection = null;
				BufferedReader reader = null;
				PrintWriter writer = null;
				try {
					readConnection = new InputStreamReader(client.getInputStream());
					reader = new BufferedReader(readConnection);
					OutputStream outStream = client.getOutputStream();
					writer = new PrintWriter(outStream);

					WebSocketHandsake(reader, writer); // Om handshake lykkes
														// legges socket til å
														// ArrayList klienter
					WebSocketClient c = new WebSocketClient(new DataOutputStream(outStream),client).setListener(WebSocket.this);
					synchronized (arrayBlock) {
						klienter.add(c);
					}
					notifyListener(c);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	/**
	 * Denne metoden tar seg av handshake med nyoppkoblede klienter.
	 * 
	 * @param reader Leseren til strømmen
	 * @param writer Skriveren til strømmen
	 * @throws Exception Kaster feil om handshaken ikke går slik den skal
	 */
	private void WebSocketHandsake(BufferedReader reader, PrintWriter writer) throws Exception {
		System.out.println("En ny webKlient prøver å koble til");

		HashMap<String, String> headers = new HashMap<String, String>();
		String requestedURI;
		double HTTPVersion;
		boolean error = false;
		String errorMessage = null;
		try {
			for (String line; (line = reader.readLine()) != null;) {
				if (line.isEmpty())
					break; // Avslutter loop når headeren er ferdig
				System.out.println("Tekst: " + line);

				if (line.startsWith("GET /")) {
					requestedURI = line.substring(4, line.indexOf(" ", 4));
					HTTPVersion = Double.parseDouble(line.substring(line.indexOf("HTTP/") + 5));
					if (HTTPVersion < 1.1) {
						throw new UnsupportedHTTPVersion("Unsupported HTTP version");
					}
					System.out.println("HTTP versjon: " + HTTPVersion);
					System.out.println("Requested URI: " + requestedURI);
				} else if (line.contains(":")) {
					headers.put(line.substring(0, line.indexOf(":")), line.substring(line.indexOf(":") + 2));
				}
			}

			// Sjekker om de grunnlegene websocket feltene er tilstedet
			if (!headers.containsKey("Host") || !headers.containsKey("Sec-WebSocket-Key")
					|| !headers.containsKey("Connection") || !headers.containsKey("Upgrade")
					|| !headers.containsKey("Sec-WebSocket-Version")) {
				throw new IllegalArgumentException("Missing header elements");
			}
		} catch (Exception e) {
			errorMessage = e.getMessage();
			error = true;
			e.printStackTrace();
		}

		// Server svarer
		try {
			if (error) {
				writer.println("status: 400");
				throw new Exception(errorMessage);
			}
			writer.write("HTTP/1.1 101 Switching Protocols\r\n" + "Upgrade: websocket\r\n" + "Connection: Upgrade\r\n"
					+ "Sec-WebSocket-Accept: " + websocketAccept(headers.get("Sec-WebSocket-Key")) + "\r\n\r\n");
			writer.flush();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Kunne ikke koble til klient");
		}
	}

	@Override
	public void close() throws IOException {
		if (socket != null) {
			try {
				socket.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Sier ifra til alle listeners av WebSocket klassen at en ny klient har
	 * koblet til
	 * 
	 * @param client
	 */
	private void notifyListener(WebSocketClient client) {
		listener.stream().forEach((lytter) -> {
			lytter.onConnection(client);
		});
	}

	/**
	 * Alle klienter bruker denne metoden når de sender en melding til serveren
	 * Varsler alle listeners om at en klient har send en medling
	 */
	@Override
	public synchronized void onMessage(JSONObject obj) {
		listener.stream().forEach((lytter) -> {
			lytter.onMessage(obj);
		});
	}

	/**
	 * Kalles når en klient frakobles
	 */
	@Override
	public void onDisconnect(WebSocketClient socket) {
		synchronized (arrayBlock) {
			if (klienter.remove(socket)) {
				System.out.println("Fjerne en klient fra listen over aktive klienter");
			} else {
				System.out.println("Mottok frakobling fra klient, men fant ikke klient i listen over aktive klienter");
			}
		}
	}
}
