package aasbo.socket;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import aasbo.exceptions.ConnectionClosedException;
import aasbo.exceptions.NotMaskedException;
import aasbo.exceptions.PingException;
import aasbo.exceptions.PongException;
import aasbo.exceptions.TooLargeMessage;
import aasbo.exceptions.UnsupportedPayloadDataException;
import aasbo.frames.FrameReader;
import aasbo.frames.FrameWriter;
import aasbo.json.JSONException;
import aasbo.json.JSONObject;
import aasbo.listeners.ClientListener;

/**
 * Denne klassen blir utdelt til hver klient som kobler til Det er "laget"
 * klientene kommuniserer med
 * 
 * @author ole-johannes & hans brenna
 *
 */
public class WebSocketClient extends Thread {

	private DataOutputStream writer;
	private ClientListener listener;
	private Socket connection;
	private FrameReader reader;

	public WebSocketClient(DataOutputStream writer, Socket socket) {
		this.writer = writer;
		this.connection = socket;
		reader = new FrameReader(connection);
		start();
	}

	/**
	 * Brukes av WebSocket til å lytte på handlinger som skjer hos hver klient
	 * 
	 * @param l
	 *            WebSocket som skal lytte
	 * @return WebSocketClient
	 */
	public WebSocketClient setListener(ClientListener l) {
		listener = l;
		return this;
	}

	/**
	 * Sender en melding/data til kun denne klienten
	 * 
	 * @param text
	 *            meldingen/data som skal sendes
	 */
	public void sendMessage(String text) {
		try {
			sendMessage(FrameWriter.ConvertString(text));
		} catch (TooLargeMessage e) {
			System.out.println("Kunne ikke skrive til klient fordi text medlingen er for stor");
			e.printStackTrace();
		}
	}

	/**
	 * Sender data til kun denne klienten
	 * 
	 * @param bytes
	 *            byte data som skal sendes (Må være formatert riktig)
	 */
	public synchronized void sendMessage(byte[] bytes) {
		try {
			writer.write(bytes);
			System.out.println("flusher");
			writer.flush();
		} catch (IOException e) {
			System.out.println("Kunne ikke skrive til klient fordi det har oppstått en feil");
			// e.printStackTrace();
			closeConnection();
		}
	}

	/**
	 * Tråd for lytting av innkommende meldinger/data fra denne klienten
	 */
	@Override
	public void run() {
		String input;
		while (true) {

			try {
				input = reader.read();
				if (input == null) {
					break;
				}
				listener.onMessage(new JSONObject(input));
			} catch (ConnectionClosedException e) {
				// e.printStackTrace();
				System.out.println(e.getMessage());
				closeConnection();
				break;
			} catch (NotMaskedException e) {
				e.printStackTrace();
			} catch (UnsupportedPayloadDataException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
				closeConnection();
				break;
			} catch (PingException e) {
				System.out.println("Mottok ping fra en klient");
				sendMessage(e.getPong());
				e.printStackTrace();
			} catch (PongException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	/**
	 * Lukker koblingen til denne klienten og varsler lytter av denne klienten
	 * om at den har koblet ned
	 */
	private void closeConnection() {
		// Kjører i ny tråd slik at "visse feil" ikke oppstår
		new Thread() {
			@Override
			public void run() {
				try {
					connection.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				listener.onDisconnect(WebSocketClient.this);
			}
		}.start();
	}
}
