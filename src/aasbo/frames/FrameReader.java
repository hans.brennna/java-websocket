package aasbo.frames;

import java.io.IOException;

import aasbo.exceptions.ConnectionClosedException;
import aasbo.exceptions.NotMaskedException;
import aasbo.exceptions.PingException;
import aasbo.exceptions.PongException;
import aasbo.exceptions.UnsupportedPayloadDataException;

import java.io.InputStream;
import java.net.Socket;

/**
 * 
 * @author hans
 * Klasse som tolker data fra en klient og responderer på disse.
 */

public class FrameReader {
	private  InputStream stream;
	private  int opcode;
	private  Socket connection;
	
	/**
	 * 
	 * @param socket tilkoblingen til klienten.
	 */
	public FrameReader(Socket socket){
		connection = socket;
	}


	/**
	 * Lytter etter meldinger fra klienten, støtter enkeltmedlinger på opptil 64kb, støtter også de aller fleste kontrollrammer samt continuation 
	 * Støtter ikke binærdata. 
	 * 
	 * @return
	 * @throws NotMaskedException kastes dersom MASK bit er satt til 0 
	 * @throws UnsupportedPayloadDataException Kastes dersom payload består av binærdata.
	 * @throws ConnectionClosedException Kastes dersom det mottas en close ramme
	 * @throws IOException 
	 * @throws PingException Kastes dersom det mottas en ping.
	 * @throws PongException Kastes dersom det mottas en pong.
	 */
	public  String read() throws NotMaskedException, UnsupportedPayloadDataException, ConnectionClosedException, IOException, PingException, PongException {

		stream = connection.getInputStream();
		
		try {
			boolean fin = false;
			
			byte current = (byte) stream.read();
			if ((current & 0x80) == 0x80) {
				fin = true;
			}

			opcode = Byte.toUnsignedInt((byte) (current & 0x0f));
			String s = null;
			if (opcode == 2) {
				throw new UnsupportedPayloadDataException("Binary data is not supported in this version");
			} else if (opcode == 1 || opcode == 0) {
				return  translate(fin);
			} else if (opcode == 8) {
				throw new ConnectionClosedException("Received a closing frame");
			} else if(opcode == 9){
				byte[] temp = translate(fin).getBytes("UTF-8");
				if(temp.length<=125){
					byte[] pongBytes = new byte[2+temp.length];
					pongBytes[0] = (byte)0x8A;
					pongBytes[1] = (byte)(temp.length);
					for(int i = 0;i<temp.length;i++){
						pongBytes[i+2]=temp[i];
					}
					throw new PingException("Received ping",pongBytes);
				}else{
					byte[] pongBytes = new byte[4+temp.length];
					pongBytes[0] = (byte)0x8A;
					pongBytes[1] =(byte)0x7E;
					pongBytes[2] = (byte)(temp.length>>8);
					pongBytes[3] = (byte)(temp.length & 0x00ff);
					for(int i = 0;i<temp.length;i++){
						pongBytes[4+i]=temp[i];
					}
					throw new PingException("Received ping",pongBytes);
				}
				
			} else if(opcode==10){
				for(int i = 0;i<5;i++){
					stream.read();
				}
				throw new PongException("Received a pong!");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 *  Hjelpemetode for å 	oversette lest data til ferdig streng.
	 * @param fin
	 * @return Ferdig dekodet payload.
	 * @throws IOException
	 * @throws NotMaskedException
	 * @throws UnsupportedPayloadDataException
	 * @throws ConnectionClosedException
	 * @throws PingException
	 * @throws PongException
	 */
	private String translate(boolean fin) throws IOException, NotMaskedException, UnsupportedPayloadDataException, ConnectionClosedException, PingException, PongException{
		String s;
		int length = 0;
		byte current = (byte) stream.read();
		if ((current & 0x80) != 0x80) {
			throw new NotMaskedException("Not masked");
		}
		length = Byte.toUnsignedInt((byte) (current & 0x7f));
		if(length>125){
			int l = stream.read();
			l = l<<8;
			l = l ^ stream.read();
			length = Short.toUnsignedInt((short)l);
		}
		System.out.println(length);

		byte[] MASK = new byte[4];
		stream.read(MASK, 0, 4);


		byte[] payload = new byte[length];
		stream.read(payload, 0, payload.length);
		byte[] decoded = new byte[length];
		for (int i = 0; i < payload.length; i++) {
			decoded[i] = (byte) (payload[i] ^ MASK[i % 4]);
		}
		s = new String(decoded, "UTF-8");
		System.out.println(s);

		if (!fin) {
			return recursiveCall(s);
		} else {
			return  s;
		}

	}
	/** Hjelpemetode for konkatenering i tilfelle continuation ramme.
	 * 
	 * @param s
	 * @return
	 * @throws NotMaskedException
	 * @throws UnsupportedPayloadDataException
	 * @throws ConnectionClosedException
	 * @throws IOException
	 * @throws PingException
	 * @throws PongException
	 */
	private  String recursiveCall(String s)
			throws NotMaskedException, UnsupportedPayloadDataException, ConnectionClosedException, IOException, PingException, PongException {
		return s + read();
	}

}
