package aasbo.frames;

import java.math.BigInteger;

import aasbo.exceptions.TooLargeMessage;
/**
 * Denne klassen lager meldinger som er støttet å sende over websockets.
 * @author ole-johannes
 *
 */
public class FrameWriter {

	/**
	 * Konverterer en String til et format som kan sendes over websocket
	 * @param text meldingen/data som skal formateres
	 * @return Et byte array med riktig formatert data
	 * @throws TooLargeMessage Om meldingen/data er for stor
	 */
	public synchronized static byte[] ConvertString(String text) throws TooLargeMessage{
		byte[] bytes = text.getBytes();
		byte opcode = (byte) 0x81;
		byte len = new Byte("00000000");
		byte[] sendes = new byte[0];
		System.out.println("text length: " + bytes.length);
		
		if(bytes.length <= 125){
			len = Byte.parseByte(String.valueOf(bytes.length));
			sendes = new byte[bytes.length+2];
			for(int i = 0; i < bytes.length; i++){
				sendes[i+2] = bytes[i];
			}
			System.out.println("bruker 1 byte");
		}
		else if(bytes.length <= 65535){
			len = (byte) 0x7E;
			sendes = new byte[bytes.length+4];
			short newlen = Short.parseShort(String.valueOf(bytes.length));
			sendes[3] = (byte)(newlen & 0xff);
			sendes[2] = (byte)((newlen >> 8) & 0xff);
			for(int i = 0; i < bytes.length; i++){
				sendes[i+4] = bytes[i];
			}
			System.out.println("bruker 2 byte");
		}
		else if(bytes.length <= Integer.MAX_VALUE){
			len = (byte) 0x7F;
			sendes = new byte[bytes.length+10];
			byte[] newlen = BigInteger.valueOf(bytes.length).toByteArray();
			sendes[2] = new Byte("00000000");
			sendes[3] = new Byte("00000000");
			sendes[4] = new Byte("00000000");
			sendes[5] = new Byte("00000000");
			for(int i = 0; i < 4-newlen.length; i++){
				sendes[6+i] = new Byte("00000000");
			}
			for(int i = 0; i < newlen.length; i++){
				sendes[10-newlen.length+i] = newlen[i];
			}
			for(int i = 0; i < bytes.length; i++){
				sendes[i+10] = bytes[i];
			}
			System.out.println("bruker 4 byte");
		}
		else{
			throw new TooLargeMessage("Meldingen er for stor");
		}
		sendes[0] = opcode;
		sendes[1] = len;
		
		return sendes;
	}
	
	/**
	 * Lager et ping byte array som kan sendes over websocket
	 * @return Et byte array med ping kommando i websocket format
	 */
	public synchronized static byte[] ping(){
		return new byte[]{(byte) 0x89, (byte) 0x0};
	}
	
	/**
	 * Lager et close byte array som kan sendes over websocket
	 * @return Et byte array med close kommando i websocket format
	 */
	public synchronized static byte[] close(){
		return new byte[]{(byte) 0x88, (byte) 0x0};
	}
}
