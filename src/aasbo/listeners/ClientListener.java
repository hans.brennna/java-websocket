package aasbo.listeners;

import aasbo.json.JSONObject;
import aasbo.socket.WebSocketClient;

public interface ClientListener {
	void onMessage(JSONObject obj);
	void onDisconnect(WebSocketClient socket);
}
