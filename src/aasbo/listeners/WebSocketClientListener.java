package aasbo.listeners;

import aasbo.json.JSONObject;
import aasbo.socket.WebSocketClient;

public interface WebSocketClientListener {
	void onMessage(JSONObject obj);
	void onConnection(WebSocketClient client);
}
